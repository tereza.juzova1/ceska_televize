# Řešení

Jsem nováčkem v oblasti webové analytiky, nejsem si jistá, že jsem postupovala zcela správně, výsledek mé prvnotní analýzy je:

Nejprve jsem prozkoumala web **iVysílání**, kde jsem se podívala do zdrojového kódu a hledala jsem fragmenty kódu, které by mi prozradily jaké nástroje webové analytiky jsou používány:

- narazila jsem na **Gemius** (Gemius měří a analyzuje chování uživatelů na internetu a poskytuje informace o návštěvnosti webových stránek, demografických datech, a jiných relevantních údajích pro online marketing a webový výzkum.)

- dále jsem narazila na **Google Analytics** pomocí gtag.js. (Global Site Tag (gtag.js) - umožňuje snadnou integraci různých nástrojů od Google, jako je Google Analytics, Google Ads, a dalších)

K analýze jednotlivých událostí a parametrů jsem využila google funkci **"Google Tag Assistant"**. ("Google Tag Assistant" je užitečný při ověřování, zda jsou sledovací tagy správně implementovány na stránkách, a může snadno odhalit problémy, jako jsou duplicitní tagy, neplatné konfigurace a další.)

Potřebovala jsem si ujasnit, jaké jsou rozdíly mezi událostmi a parametry:

- **události** jsou tedy konkrétní akce, které uživatelé dělají: sledování videa, kliknutí na tlačítko, procházení stránek atd.
    - pomáhají pochopit chování uživatelů na webu

- **parametry** jsou spojené s událostmi, představují dodatečné informace k detailnější analýze
    - například pro událost sledování videa mohou existovat parametry: čas sledování videa, stupeň sledování videa, tento parametr může být užitečný, zda uživatel dokouká video až do konce, interakce s ovládacími prvky (play, pause, stop), dale pak například title videa nebo kvalitu videa

Pomocí [tagassistant](https://tagassistant.google.com/ ) jsem narazila na tyto události:

- player_pause
- player_play
- player_settings_change_audio
- player_ad_skip
- player_ad_play
- player_load

Každý z těchto eventů má x parametrů:
- obecné parametry (příklady):
    - Language
    - User-Agent Architecture
    - User-Agent Bitness
    - User-Agent Platform

- specifické parametry (příklady):    
    - player_settings
    - player_time
    - player_ad_title
    - player_ad_type
    - player_ad_time_length



```

Nedokázala jsem se dostat k debuggingu pro seriál Kosmo, nemohla jsem tedy přímo porovnat využívané parametry :(

```
Napadají mě obecné události/parametry u seriálu, které by bylo dobré sledovat:

- kolik diváků sleduje všechny epozody série až do konce
- pauza mezi sledovanými epizodami - mohlo by být dobré pro plánování nových vydání
- přeskakování intra - kolik diváků přeskakuje úvod epizody
- průměrná délka sledování epizody - jak dlouho diváci sledují jednu epizodu seriálu - jestli si jí například nepouštějí po částech
- návratnost uživatelů

Obohatila bych videa o možnost komentování a hodnoceni přímo pod videem. Jako datař bych ráda propojovala data z google analytics s dalšími možnými zdroji. V dnešní době je velmi důležité propojenost se sociálnímí sítěmi, např. kolik lidí přišlo z instagramu/facebooku na web ivysilání. Dále pak heatmapy a A/B testování (vizualizace klikání, analýza skrolování, zjištění blind spotů atd.).