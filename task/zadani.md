# Zadání
Zmapujte měření webového přehrávače [iVysílání](https://www.ceskatelevize.cz/ivysilani ):


1. Jaké události se měří
2. Jaké parametry se měří
3. Jak se liší parametry u živého přenosu ČT1 VS seriálu Kosmo.
4. Jak se liší parametry u seriálu Kosmo VS filmu Samotáři
5. Bonusová otázka: co byste navrhl/a na zlepšení? Kde vidíte mezery?